package tasks.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TasksOperationsTest {
    TasksOperations tasksOperations;
    SimpleDateFormat sdf;

    @Test
    void TC01_WBT() {
        try {
            List<Task> tasksList = new ArrayList<>();
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            Date start = sdf.parse("2020-02-25 01:20");
            Date end = sdf.parse("2020-02-25 02:20");
            Task task = new Task("task1", start, end, 1);
            tasksList.add(task);

            start = sdf.parse("2020-03-25 10:30");
            end = sdf.parse("2020-03-25 11:30");
            task = new Task("task2", start, end, 1);
            tasksList.add(task);

            start = sdf.parse("2021-03-25 10:30");
            end = sdf.parse("2021-03-25 11:30");
            task = new Task("task3", start, end, 1);
            tasksList.add(task);

            ObservableList<Task> tasks = FXCollections.observableArrayList(tasksList);
            tasksOperations = new TasksOperations(tasks);

            start = sdf.parse("2020-03-25 09:30");
            end = sdf.parse("2020-03-25 12:30");
            Iterable<Task> tasksNew = tasksOperations.incoming(start, end);

            start = sdf.parse("2020-03-25 10:30");
            end = sdf.parse("2020-03-25 11:30");
            Task taskNew = new Task("task2", start, end, 1);
            ArrayList<Task> expectedTasks = new ArrayList<>();
            expectedTasks.add(taskNew);

            assert tasksNew.equals(expectedTasks);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    void TC02_WBT() {
        try {
            List<Task> tasksList = new ArrayList<>();
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            ObservableList<Task> tasks = FXCollections.observableArrayList(tasksList);
            tasksOperations = new TasksOperations(tasks);

            Date start = sdf.parse("2020-03-25 09:30");
            Date end = sdf.parse("2020-03-25 12:30");
            Iterable<Task> tasksI = tasksOperations.incoming(start, end);

            ArrayList<Task> expectedTasks = new ArrayList<>();

            assert tasksI.equals(expectedTasks);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}