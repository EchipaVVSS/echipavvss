package tasks.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tasks.view.Main;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TaskIOTest {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    LinkedTaskList tasksList = new LinkedTaskList();

    @Test
    void TC01_EC() {
        try {
            String title = "Title";
            Date time = sdf.parse("2021-03-30 17:00");
            Task task = new Task(title, time);
            tasksList.add(task);
            TaskIO.writeBinary(tasksList, Main.savedTasksFile);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void TC02_EC(){
        Assertions.assertThrows(IOException.class, () -> {
            String title = "";
            Date time = sdf.parse("2021-03-30 17:00");
            Task task = new Task(title, time);
            tasksList.add(task);
            TaskIO.writeBinary(tasksList, Main.savedTasksFile);
        });
    }

    @Test
    void TC03_EC(){
        try {
            String title = "Title";
            Date start = sdf.parse("2021-03-31 17:00");
            Date end = sdf.parse("2021-04-31 17:00");
            int interval = 1;
            Task task = new Task(title, start, end, interval);
            tasksList.add(task);
            TaskIO.writeBinary(tasksList, Main.savedTasksFile);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void TC04_EC(){
        Assertions.assertThrows(IOException.class, () -> {
            String title = "Title";
            Date start = sdf.parse("2021-02-30 17:00");
            Date end = sdf.parse("2021-04-31 17:00");
            int interval = 1;
            Task task = new Task(title, start, end, interval);
            tasksList.add(task);
            TaskIO.writeBinary(tasksList, Main.savedTasksFile);
        });
    }

    @Test
    void TC05_EC(){
        try {
            String title = "Title";
            Date start = sdf.parse("2021-03-31 17:00");
            Date end = sdf.parse("2021-04-31 17:00");
            int interval = 1;
            Task task = new Task(title, start, end, interval);
            tasksList.add(task);
            TaskIO.writeBinary(tasksList, Main.savedTasksFile);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void TC06_EC(){
        Assertions.assertThrows(IOException.class, () -> {
            String title = "Title";
            Date start = sdf.parse("2021-04-01 17:00");
            Date end = sdf.parse("2021-03-29 17:00");
            int interval = 1;
            Task task = new Task(title, start, end, interval);
            tasksList.add(task);
            TaskIO.writeBinary(tasksList, Main.savedTasksFile);
        });
    }

    @Test
    void TC07_BVA() {
        try {
            String title = "Task pentru curatenie";
            Date start = sdf.parse("2020-03-20 13:00");
            Date end = sdf.parse("2020-03-21 11:00");
            int interval = 1;
            Task task = new Task(title, start, end, interval);
            tasksList.add(task);
            TaskIO.writeBinary(tasksList, Main.savedTasksFile);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void TC08_BVA(){
        Assertions.assertThrows(ParseException.class, () -> {
            String title = "Task pentru curatenie";
            Date time = sdf.parse("asdasdasd");
            Task task = new Task(title, time);
            tasksList.add(task);
            TaskIO.writeBinary(tasksList, Main.savedTasksFile);
        });
    }

    @Test
    void TC09_BVA() {
        try {
            String title = "Task pentru curatenie";
            Date start = sdf.parse("2020-03-20 13:00");
            Date end = sdf.parse("2020-03-21 11:00");
            int interval = 1;
            Task task = new Task(title, start, end, interval);
            tasksList.add(task);
            TaskIO.writeBinary(tasksList, Main.savedTasksFile);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void TC10_BVA(){
        Assertions.assertThrows(IOException.class, () -> {
            String title = "Task pentru curatenie";
            Date start = sdf.parse("2020-03-20 13:00");
            Date end = sdf.parse("2020-33-20 25:61");
            int interval = 1;
            Task task = new Task(title, start, end, interval);
            tasksList.add(task);
            TaskIO.writeBinary(tasksList, Main.savedTasksFile);
        });
    }
}